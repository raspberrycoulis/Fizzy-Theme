# CHANGELOG

## [v0.1.1](https://github.com/huangyuzhang/Fizzy-Theme/releases/tag/v0.1.1) (2019.04.15)
### NEW
- **home**: exclude posts with `#noindex` tag
### MOD 
- **post**: paragraph style; code block style; list style

## [v0.1.0](https://github.com/huangyuzhang/Fizzy-Theme/releases/tag/v0.1.0) (2019.04.14)
### NEW
- **template**: tag archive template ([demo](https://fizzy.cc/tag/)) (close: [#2](https://github.com/huangyuzhang/Fizzy-Theme/issues/2))
- **post**: image support (size: regular, wide, full-width)
- **post**: content gallery support
- **post**: embedded content support
### MOD 
- **post**: blockquote style (close: [#1](https://github.com/huangyuzhang/Fizzy-Theme/issues/1))
- **post**: headings size (H1,H2,H3,H4,H5,H6) (close: [#1](https://github.com/huangyuzhang/Fizzy-Theme/issues/1))
- **global**: link hover transition style
- **post**: author box fixed with a avatar placeholder (close: [#1](https://github.com/huangyuzhang/Fizzy-Theme/issues/1))
### DEL
- unused templates and images

## [v0.0.1](https://github.com/huangyuzhang/Fizzy-Theme/releases/tag/v0.0.1) (2019.04.12)
- First release
